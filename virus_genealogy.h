#ifndef VIRUS_GENEALOGY_H
#define VIRUS_GENEALOGY_H
#include <iostream>
#include <unordered_map>
#include <set>
#include <vector>
#include <memory>
#include <exception>

struct VirusNotFound : public std::exception {
	const char * what() const throw() {
		return "VirusNotFound";
	}
};

struct VirusAlreadyCreated : public std::exception {
	const char * what() const throw() {
		return "VirusAlreadyCreated";
	}
};

struct TriedToRemoveStemVirus : public std::exception {
	const char * what() const throw() {
		return "TriedToRemoveStemVirus";
	}
};

template <class Virus>
class VirusGenealogy {
public:
	VirusGenealogy(typename Virus::id_type const &stem_id);
    VirusGenealogy & operator=(const VirusGenealogy&) = delete;
    VirusGenealogy(const VirusGenealogy&) = delete;
    typename Virus::id_type get_stem_id() const {
		return stem_id;
    }
    bool exists(typename Virus::id_type const &id) const {
		return nodes.count(id);
	}

    std::vector<typename Virus::id_type> get_children(typename Virus::id_type const &id) const;
	std::vector<typename Virus::id_type> get_parents(typename Virus::id_type const &id) const;
	void create(typename Virus::id_type const &id, typename Virus::id_type const &parent_id);
	void create(typename Virus::id_type const &id, std::vector<typename Virus::id_type> const &parent_ids);
	void connect(typename Virus::id_type const &child_id, typename Virus::id_type const &parent_id);
	void remove(typename Virus::id_type const &id);
	Virus& operator[](typename Virus::id_type const &id) const;

private:

	std::unordered_map<typename Virus::id_type, std::shared_ptr<Virus>> nodes;
	std::unordered_map<typename Virus::id_type, std::set<typename Virus::id_type>> parents;
	std::unordered_map<typename Virus::id_type, std::set<typename Virus::id_type>> children;
	typename Virus::id_type const stem_id;
};

template <class Virus>
VirusGenealogy<Virus>::VirusGenealogy(typename Virus::id_type const &stem_id):
	stem_id(stem_id) {

	nodes[stem_id] = std::make_shared<Virus>(Virus(stem_id));
	parents[stem_id] = std::set<typename Virus::id_type>();
	children[stem_id] = std::set<typename Virus::id_type>();
}

template <class Virus>
std::vector<typename Virus::id_type>
VirusGenealogy<Virus>::get_children(typename Virus::id_type const &id) const{
	if (!exists(id)) {
        throw VirusNotFound();
	}

	auto it = children.find(id);
	std::vector<typename Virus::id_type> id_vector_children(it->second.begin(), it->second.end());
	return id_vector_children;
}

template <class Virus>
std::vector<typename Virus::id_type>
VirusGenealogy<Virus>::get_parents(typename Virus::id_type const &id) const{
	if (!exists(id)) {
        throw VirusNotFound();
	}

	auto it = parents.find(id);
	std::vector<typename Virus::id_type> id_vector_parents(it->second.begin(), it->second.end());
	return id_vector_parents;
}

template <class Virus>
Virus& VirusGenealogy<Virus>::operator[](typename Virus::id_type const &id) const {
	if (!exists(id)) {
        throw VirusNotFound();
	}
	auto it = nodes.find(id);
    return *(it->second);
}

template <class Virus>
void VirusGenealogy<Virus>::create(typename Virus::id_type const &id, typename Virus::id_type const &parent_id) {
	if (exists(id)) {
        throw VirusAlreadyCreated();
	}
	if (!exists(parent_id)) {
		throw VirusNotFound();
	}

	nodes[id] = std::make_shared<Virus>(Virus(id)); //stworzenie nowego wezla

    std::set<typename Virus::id_type> new_parents;
    new_parents.insert(parent_id); //stworzenie listy ojcow temu wezlowi
    children[parent_id].insert(id);//dodanie ojcowi nowego syna

    parents[id] = new_parents;
    children[id] = std::set<typename Virus::id_type>();
}

template <class Virus>
void VirusGenealogy<Virus>::create(typename Virus::id_type const &id, std::vector<typename Virus::id_type> const &parent_ids) {
	if (exists(id)) {
        throw VirusAlreadyCreated();
	}
	for (auto it = parent_ids.begin(); it != parent_ids.end(); ++it) {
		if (!exists(*it)) {
			throw VirusNotFound();
		}
	}

	nodes[id] = std::make_shared<Virus>(Virus(id)); //stworzenie nowego wezla

    std::set<typename Virus::id_type> new_parents;
    for (auto it = parent_ids.begin(); it != parent_ids.end(); ++it) {
		new_parents.insert(*it); //tworzenie listy ojcow temu wezlowi
		children[*it].insert(id); //dodanie ojcowi nowego syna
    }

    parents[id] = new_parents;
    children[id] = std::set<typename Virus::id_type>();
}

template <class Virus>
void VirusGenealogy<Virus>::connect(typename Virus::id_type const &child_id, typename Virus::id_type const &parent_id) {
	if (!exists(child_id) || !exists(parent_id)) {
        throw VirusNotFound();
	}

    parents[child_id].insert(parent_id);
	children[parent_id].insert(child_id);
}

template <class Virus>
void VirusGenealogy<Virus>::remove(typename Virus::id_type const &id) {
	if (!exists(id)) {
		throw VirusNotFound();
	}
	if (id == stem_id) {
		throw TriedToRemoveStemVirus();
	}

    auto &children_set = children[id];
    auto &parents_set = parents[id];

	//ojcom uswam syna
	for (auto it = parents_set.begin(); it != parents_set.end(); ++it) {
        children[*it].erase(id);
	}

    //dzieciom usuwam jednego ojca. jesli nie ma ojcow to usuwam dziecko
    for (auto it = children_set.begin(); it != children_set.end(); ++it) {
        parents[*it].erase(id);
        if (parents[*it].size() == 0) {
			remove(*it);
        }
	}

	//teraz sam musze sie unicestwic;
	nodes.erase(id);
	parents.erase(id);
	children.erase(id);
}
#endif // VIRUS_GENEALOGY_H
